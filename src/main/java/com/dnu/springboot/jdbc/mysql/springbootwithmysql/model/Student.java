package com.dnu.springboot.jdbc.mysql.springbootwithmysql.model;

import java.time.LocalDate;

public class Student {
    Integer id;
    String firstName;
    String secondName;
    LocalDate date;
}
