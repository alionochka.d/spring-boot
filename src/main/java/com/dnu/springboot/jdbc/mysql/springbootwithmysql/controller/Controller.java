package com.dnu.springboot.jdbc.mysql.springbootwithmysql.controller;


import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

public class Controller {
        @RequestMapping(value = "/greeting")
        public String controller(@RequestParam(name = "name", required = false, defaultValue = "World") String name, Model model) {
            model.addAttribute("name", name);
            return "greeting";
        }

}
