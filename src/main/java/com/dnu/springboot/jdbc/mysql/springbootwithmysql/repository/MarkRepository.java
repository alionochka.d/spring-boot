package com.dnu.springboot.jdbc.mysql.springbootwithmysql.repository;
import com.dnu.springboot.jdbc.mysql.springbootwithmysql.model.Mark;

import javax.management.Query;
import java.sql.*;
import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.Statement;
import java.sql.ResultSet;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;


public class MarkRepository {
    public static void save(Mark mark) throws  Exception{
        Connection connection  = DriverManager.getConnection(
                "jdbc:mysql://localhost:3306/University",
                "root", "rpas");
        PreparedStatement preparedStatement = null;
        String query = "insert into mark " + "(id, mark, dateOfMark, studentId, lecturerId, subjectId) " + "VALUES(?, ?, ?, ?, ?, ?)";

        try{

            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, mark.id);
            preparedStatement.setInt(2, mark.mark);
            preparedStatement.setDate(3, Date.valueOf(mark.dateOfMark));
            preparedStatement.setInt(4, mark.studentId);
            preparedStatement.setInt(5, mark.lecturerId);
            preparedStatement.setInt(6, mark.subjectId);
            preparedStatement.executeUpdate();


            preparedStatement.close();

            System.out.println("Record inserted successfully.");
        } catch(Exception e){
            e.printStackTrace();
        }

    }

    public static List<Mark> findAll() throws  Exception {
        String query = "SELECT * FROM mark";
        Connection connection = DriverManager.getConnection(
                "jdbc:mysql://localhost:3306/University",
                "root", "rpas");

        List<Mark> markList = new ArrayList<>();
        try (Statement statement = connection.createStatement()) {
            ResultSet rs = statement.executeQuery(query);

            while (rs.next()) {
                Mark mark = new Mark();
                mark.id = rs.getInt("id");
                mark.mark = rs.getInt("mark");
                mark.dateOfMark = rs.getDate("dateOfMark").toLocalDate();
                mark.studentId = rs.getInt("studentId");
                mark.lecturerId = rs.getInt("lecturerId");
                mark.subjectId = rs.getInt("subjectId");
                markList.add(mark);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return markList;
    }
}
